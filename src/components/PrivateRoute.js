import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {userService} from '../_services/userservice';


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
    sessionStorage.getItem('token') && sessionStorage.getItem('user_id') ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default PrivateRoute;
