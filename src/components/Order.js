import React, { Component } from 'react';
import OrderItem from './OrderItem';

const URL_ORDERS = process.env.REACT_APP_ORDER_API_URL;


class Order extends Component {
    constructor(props) {
        super(props);
        this.state = { 'orders': [] }
    }

    async getOrders() {
        const userID = sessionStorage.getItem('user_id');
        const response = await fetch(
            URL_ORDERS,
            {
                method: 'GET',
                headers: { 'Content-Type': 'application/json' },
            }
        );

        const jsonData = await response.json();

        this.setState({'orders': jsonData.orders});

    }

    componentDidMount() {
        this.getOrders();
    }

    render() {
        const {orders} = this.state;
        if (orders.length > 0 ){
            return (
                <div className="container-fluid">
                    {orders.map((item, i) => (
                        <OrderItem key={i} item={item} />
                    ))}
                </div>
            );
        }
        else{
            return (
                <div className="container-fluid text text-center">
                    <h3>Nenhum pedido encontrado.</h3>
                </div>
            )
        }
        
    }
}

export default Order;
