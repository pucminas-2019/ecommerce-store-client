import React, { Component } from 'react';
import { ProductConsumer } from "../context";
import { Redirect } from 'react-router-dom';
import CartList from "./Cart/CartList";
import EmptyCart from "./Cart/EmptyCart";


const URL_ORDERS = process.env.REACT_APP_ORDER_API_URL

class Checkout extends Component{
  constructor(props){
    super(props);
    this.state = {cart: [], ordersSent: false}
  }


  async submitCheckoutItems (data) {
    let cartSession = sessionStorage.getItem('cart');
    let userId = sessionStorage.getItem('user_id');
    let products =  JSON.parse(cartSession);
    
    if (!userId)
      return <Redirect to= "/login" />
    
    const userProducts = {'data' : {'user_id': userId, products}};

    const response = await fetch(
      URL_ORDERS,
      {
        method: 'post',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify(userProducts) 
      }
    );

    const jsonData = await response.json();

    if (jsonData.orders) {
      sessionStorage.removeItem('cart');
      this.setState({ordersSent:true});     
    }
  }
  
  render() {
    return(
      <section>
        <ProductConsumer>
          {value => {            
            const { cart } = value;            
            if (cart.length > 0) {
              return (
                <React.Fragment>
                  <CartList value={value} />
                  <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-capitalize text-right">
                    <button
                      className="btn btn-success text-uppercase mb-3 px-5"
                      type="button"
                      onClick={() => this.submitCheckoutItems(value.cart)}
                    >
                    Enviar 
                    </button>
                  </div>
                  {this.state.ordersSent && <p className='text-sm-center text-danger'>Enviado</p>}
                </React.Fragment>
              );
            } else {
              return <EmptyCart />;
            }
          }}
        </ProductConsumer>
      </section>
    )
  }
}

export default Checkout;
