import React, { Component } from 'react';



class OrderItem extends Component {

    render() {
        const { status, total, products, created_at } = this.props.item
        return (

            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">Status: {status}</h3>
                </div>
                <div className="panel-body">
                    <p>Total do pedido: {total}</p>
                    <p>Data do pedido: {created_at}</p>
                    <span>Produtos: {products.map((product, i) => (
                        <div key={i}>
                            <span>{i} - Modelo: {product.title}</span>
                            <span>marca: {product.company}</span>
                            <span>preço: {product.price}</span>
                            <span>quantidade: {product.count}</span>
                        </div>
                    ))}</span>
                </div>
            </div>

        )
    }
}

export default OrderItem;