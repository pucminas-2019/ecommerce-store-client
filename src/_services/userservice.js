import { authHeader } from '../_helpers/auth-header';

const AUTH_API_URL = process.env.REACT_APP_AUTH_API_URL

export const userService = {
    login,
    logout,
    isAuthenticated
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(AUTH_API_URL, requestOptions)
        .then(handleResponse)
        .then(data => {
            // login successful if there's a user in the response
            if (data.token) {
                // store user details and basic auth credentials in local storage 
                // to keep user logged in between page refreshes
                data.authdata = window.btoa(username + ':' + password);
                sessionStorage.setItem('token', data.token);
                sessionStorage.setItem('user_id', data.user_id);
            }
            return data;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}


function isAuthenticated() {
  let token = localStorage.getItem('user');
  
  if (!token) {
    return false;
  }
  
  const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 'access_token': token })
    };

   fetch(AUTH_API_URL, requestOptions)
    .then(res => {
      if (res.status === 200) {
        return true;
      }
      return false;
    }) 
    .catch(error => console.log('error:', error));

}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                //window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
